import tensorflow as tf
from keras.layers import Input, Embedding, Dense, Dropout, Layer, MultiHeadAttention, LayerNormalization
from keras.optimizers.schedules import LearningRateSchedule
import numpy as np

def positional_encoding(model_size, sequence_length):
    output = []
    # for each sequence we create a vector of size model_size and give their positional argument
    for pos in range(sequence_length):
        PE = np.zeros((model_size))
        # print(PE.shape)
        for i in range(model_size):
            if i%2==0:
                PE[i] = np.sin(pos/(10000 ** (i/model_size)))
            else:
                PE[i] = np.cos(pos/(10000 **((i-1)/model_size)))
        output.append(tf.expand_dims(PE, axis=0))
    # print(len(output))
    out = tf.concat(output, axis=0)
    # print(len(output), len(output[0]))
    out = tf.expand_dims(out, axis=0)
    return tf.cast(out, dtype=tf.float32)

class CustomSelfAttention(Layer):
    def __init__(self, model_size):
        super(CustomSelfAttention, self).__init__()
        self.model_size = model_size
    def call(self, query, key, value, masking):
        ## compute scores
        score = tf.matmul(query, key, transpose_b=True)
        ## scaling
        score /= tf.math.sqrt(tf.cast(self.model_size, tf.float32))
        ## masking
        masking = tf.cast(masking, dtype=tf.float32)
        score += (1.-masking)*1e-10
        ## attention_weights
        attention = tf.nn.softmax(score, axis=1)*masking
        ## output
        head = tf.matmul(attention, value)
        return head
    
class CustomMultiHeadAttention(Layer):
    def __init__(self, num_heads, key_dim):
        super(CustomMultiHeadAttention, self).__init__()

        self.num_heads = num_heads
        self.dense_q = [Dense(key_dim//num_heads) for _ in range(num_heads)]
        self.dense_k = [Dense(key_dim//num_heads) for _ in range(num_heads)]
        self.dense_v = [Dense(key_dim//num_heads) for _ in range(num_heads)]
        self.dense_o = Dense(key_dim)
        self.self_attention = CustomSelfAttention(key_dim)

    def call(self, query, key, value, attention_mask):
        heads = []

        for i in range(self.num_heads):
            head = self.self_attention(self.dense_q[i](query), self.dense_k[i](key),
                                       self.dense_v[i](value), attention_mask)
            heads.append(head)

        heads = tf.concat(heads, axis=2)
        heads = self.dense_o(heads)
        return heads
    



class Embeddings(Layer):
    def __init__(self, sequence_length, vocab_size, embed_dim):
        super(Embeddings, self).__init__()
        self.token_embedding = Embedding(input_dim=vocab_size, output_dim=embed_dim) # embedding layer to create sequence, embedding_dim output
        self.sequence_length = sequence_length
        self.vocab_size = vocab_size
        self.embed_dim = embed_dim

    def call(self, inputs):
        embedded_tokens = self.token_embedding(inputs) # here seq_len, embedding_dim is outut
        embedded_position =positional_encoding(self.embed_dim, self.sequence_length) # create positional argument for each embeddind_dim position for each sequence length

        return embedded_tokens + embedded_position # for each batch this postional encoding is added
    
    def compute_mask(self, inputs, mask = None): # compute mask will be used in decoder block
        return tf.math.not_equal(inputs, 0)# it will output same as inputs but here zero inputs will be false and non zero true
    
class TransformerEncoder(Layer):
    def __init__(self, embed_dim, dense_dim, num_heads):
        super(TransformerEncoder, self).__init__()
        self.embed_dim = embed_dim
        self.dense_dim = dense_dim
        self.num_heads = num_heads
        self.attention = CustomMultiHeadAttention(num_heads=num_heads, key_dim=embed_dim)

        self.dense_proj = tf.keras.Sequential(
            [
                Dense(dense_dim, activation='relu'),
                Dense(embed_dim)
            ]
        )

        self.layernorm_1 = LayerNormalization()
        self.layernorm_2 = LayerNormalization()
        self.supports_masking = True

    def call(self, inputs, mask=None):
        # print(mask)
        if mask is not None:
            mask = tf.cast(
                mask[:, tf.newaxis, :], dtype='int32'
            )
            # print(padding_mask.shape)
            T = tf.shape(mask)[2]
            padding_mask = tf.repeat(mask, T, axis=1)
            # print(padding_mask)

        attention_output = self.attention(query=inputs, key=inputs, value=inputs, attention_mask=padding_mask)

        proj_input = self.layernorm_1(inputs + attention_output)
        proj_output = self.dense_proj(proj_input)
        return self.layernorm_2(proj_input+proj_output)
    

class TransformerDecoder(Layer):
    def __init__(self, embed_dim, latent_dim, num_heads):
        super(TransformerDecoder, self).__init__()
        self.embed_dim = embed_dim
        self.latent_dim = latent_dim
        self.num_heads = num_heads
        self.attention_1 = CustomMultiHeadAttention(num_heads=num_heads, key_dim=embed_dim)
        self.attention_2 = CustomMultiHeadAttention(num_heads=num_heads, key_dim=embed_dim)
        self.dense_proj = tf.keras.Sequential(
            [Dense(latent_dim, activation='relu'),
             Dense(embed_dim)]
        )
        self.layernorm_1 = LayerNormalization()
        self.layernorm_2 = LayerNormalization()
        self.layernorm_3 = LayerNormalization()
        self.supports_masking = True

    def call(self, inputs, encoder_outputs, enc_mask, mask = None):
        if mask is not None:
            casual_mask = tf.linalg.band_part(
                tf.ones([  
                    tf.shape(inputs)[0],
                    tf.shape(inputs)[1],
                    tf.shape(inputs)[1]
                ], dtype=tf.int32), -1, 0
            )

            mask = tf.cast(mask[:, tf.newaxis, :], dtype='int32')
            enc_mask = tf.cast(enc_mask[:, tf.newaxis, :], dtype='int32')

            T = tf.shape(mask)[2]

            padding_mask = tf.repeat(mask, T, axis = 1)
            cross_attn_mask = tf.repeat(enc_mask, T, axis=1)
            combined_mask = tf.minimum(padding_mask, casual_mask)
            # print('padding', padding_mask)
            # print('casual', casual_mask)
            # print('crossattn', cross_attn_mask)
            # print('combined_mask', combined_mask)

        attention_output_1 = self.attention_1(query=inputs, key=inputs, value=inputs, attention_mask = combined_mask)

        out_1 = self.layernorm_1(inputs + attention_output_1)

        attention_output_2 = self.attention_2(
            query=out_1, key=encoder_outputs, value=encoder_outputs, attention_mask = cross_attn_mask
        )

        out_2 = self.layernorm_2(out_1 + attention_output_2)

        proj_output = self.dense_proj(out_2)

        return self.layernorm_3(out_2 + proj_output)

    

class Scheduler(LearningRateSchedule):
    def __init__(self, d_model, warmup_steps):
        super(Scheduler, self).__init__()
        self.d_model = tf.cast(d_model, tf.float64)
        self.warmup_steps = tf.cast(warmup_steps, dtype=tf.float64)

    def __call__(self, step):
        step = tf.cast(step, dtype=tf.float64)
        return (self.d_model**(-0.5))*tf.math.minimum(step**(-0.5), step*(self.warmup_steps ** -1.5))
    
def Transformer(input_lang_sequence, output_lang_sequence, vocabulary_size, embedding_dim, dense_dim, num_heads, num_layers):

  encoder_inputs=Input(shape=(None,), dtype="int64", name="input_1")
  emb = Embeddings(input_lang_sequence,vocabulary_size,embedding_dim)
  x = emb(encoder_inputs)
  enc_mask = emb.compute_mask(encoder_inputs)

  for _ in range(num_layers):
    x=TransformerEncoder(embedding_dim,dense_dim,num_heads)(x)
  encoder_outputs=x

  decoder_inputs=Input(shape=(None,), dtype="int64", name="input_2")

  x = Embeddings(output_lang_sequence,vocabulary_size,embedding_dim)(decoder_inputs)
  for i in range(num_layers):
    x=TransformerDecoder(embedding_dim,dense_dim,num_heads)(x, encoder_outputs,enc_mask)
  x=Dropout(0.5)(x)
  decoder_outputs=Dense(vocabulary_size, activation="softmax")(x)

  transformer = tf.keras.Model(
      [encoder_inputs, decoder_inputs], decoder_outputs, name="transformer"
  )

  return transformer